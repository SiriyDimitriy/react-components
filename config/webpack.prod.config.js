const config = require('./webpack.common.config.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

config.entry = [
    './entry.js',
    // './components/simple/Button/Button.jsx',
];

config.mode = 'production';

config.optimization = {
    minimizer: [
        // we specify a custom UglifyJsPlugin here to get source maps in production
        new UglifyJsPlugin({
            cache: true,
            parallel: true,
            uglifyOptions: {
                warnings: false,
                compress: {
                    conditionals: true,
                    unused: true,
                    comparisons: true,
                    sequences: true,
                    dead_code: true,
                    evaluate: true,
                    if_return: true,
                    join_vars: true,
                    drop_console: true, //remove console.log
                    // unsafe: true,
                    inline: false,
                    passes: 2,
                    keep_fargs: false,
                },
                output: {
                    comments: false,
                    beautify: false,
                },
                ecma: 6,
                mangle: true,
            },
            sourceMap: true,
        }),
    ],
};

module.exports = config;