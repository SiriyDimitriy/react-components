import React from 'react';
import ButtonStyles from './Button.less';
import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';
import Loader from '../Loader/Loader';
import classNames from 'classnames/bind';

const appContext = process.env.APPLICATION_CONTEXT;

let cx = classNames.bind(ButtonStyles);

class ActionButton extends React.Component {
    constructor(props) {
        super(props);

        this.onClickHandler = this.onClickHandler.bind(this);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {loading} = this.props;

        if (nextProps.loading && !nextProps.path && nextProps.loading !== loading && this.btn) {
            const style = this.btn.currentStyle || window.getComputedStyle(this.btn);
            this.btn.style.width = style.width;
        }
        if (!nextProps.loading && !nextProps.path && nextProps.loading !== loading && this.btn) {
            this.btn.style.width = 'auto';
        }
    }

    onClickHandler(e) {
        const {onClick} = this.props;

        e.preventDefault(); //prevent unexpected behavior provided by old application
        onClick && onClick();
    }

    render() {
        const {path, type, text, actionType, loading, disabled, iconUrl, className, iconClassName, reactDndDragProps} = this.props;
        let iconClass = ButtonStyles.icon;
        let btnClass = cx({
            'button': true,
            'button_primary': !type,
            'button_success': type === 'submit' || type === 'success',
            'button_default': type === 'default',
            'button_disabled': disabled,
            'button_clear': type === 'clear',
            'button_icon': type === 'icon',
        });

        if (className) {
            btnClass = `${btnClass} ${className}`;
        }

        if (type === 'icon' && iconClassName) {
            iconClass = `${iconClass} ${iconClassName}`;
        }

        return (
            // path
            //     ? <Link className={btnClass} to={path}>{text}</Link>
            <button className={btnClass}
                    disabled={loading || disabled}
                    onClick={this.onClickHandler}
                    type={actionType}
                    ref={r => this.btn = r}
                    {...reactDndDragProps}>
                {loading ? <Loader radius='15px' color='white'/> : text}
                {iconUrl && <img src={`/${appContext}${iconUrl}`} alt={iconUrl} className={iconClass}/>}
            </button>
        );
    }
}

export default ActionButton;

ActionButton.propTypes = {
    path: PropTypes.string,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    actionType: PropTypes.string,
    iconUrl: PropTypes.string,
    onClick: PropTypes.func,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    iconClassName: PropTypes.string,
};
