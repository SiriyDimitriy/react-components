import React from 'react';
import {DragDropContext, Draggable, Droppable} from 'react-beautiful-dnd';
import TableStyles from './DnDTable.less';
import TableColumn from './TableColumn/TableColumn';

const resizeTableColumn = (table, columnClassName, resizeToValue, minWidth) => {
    const currentColumnCellsClassname = table.getElementsByClassName(columnClassName);

    if (resizeToValue > minWidth) {
        for (let i = 0; i < currentColumnCellsClassname.length; i++) {
            let currentCell = currentColumnCellsClassname.item(i);

            currentCell.style.minWidth = resizeToValue + 'px';
            currentCell.style.maxWidth = resizeToValue + 'px';
        }
    }
};

class DndTable extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            isDndDisabled: false,
            isDraggingStarted: false,
            // styleObject: localStorage.getItem('tableStyles') ? JSON.parse(localStorage.getItem('tableStyles')) : {},
            styleObject: {},
        };

        // console.log('CustomTableComponent constructor ' + performance.now() + ' milliseconds.');
    }

    componentDidMount() {
        const {resizable} = this.props;
        let columns = document.getElementById('search_results_table').childNodes;
        let leftDist = 0;
        let makeFixable = true;
        let isFirstColumn = false;
        for (let i = 0, l = columns.length; i < l; i++) {
            const currentNodeWidth = window.getComputedStyle(document.querySelector(`#search_results_table > div:nth-child(${i + 1}`)).width;

            if (makeFixable) {
                columns[i].style.position = 'sticky';
                columns[i].style.left = `${leftDist}px`;
                columns[i].style.zIndex = (i === 0) ? 11 : 10; // tooltip issue for column of numbers
                leftDist = leftDist + parseFloat(currentNodeWidth);
            }

            if (columns[i].getAttribute('data-key') === 'name') {
                makeFixable = false;
            }
        }

        //resizing
        let thElm;
        let startOffset;
        let current = document.getElementById('search_results_table');
        if (current && resizable) {
            Array.prototype.forEach.call(

                current.getElementsByClassName('table_column'),
                (col) => {
                    isFirstColumn = false;
                    // col.style.position = 'relative';
                    if (this.state) {
                        const columnClassName = col.className.split(' ').find(el => el.indexOf('column-') !== -1);
                        const {[columnClassName]: column} = this.state;
                        if(columnClassName === 'column-0'){
                            isFirstColumn =true;
                        }

                        if (column && column.currentWidth) {

                            resizeTableColumn(current, columnClassName, column.currentWidth, 60);
                        }

                    }
                    if(!isFirstColumn){
                    const grip = col.querySelectorAll('[data-element=resizable]');

                    if (grip.length !== 0) {
                        grip[0].addEventListener('mousedown', (e) => {
                            thElm = col;
                            startOffset = col.offsetWidth - e.pageX;
                        });
                        grip[0].addEventListener('mouseover', () => {
                            this.setState({isDndDisabled: true});
                        });
                        grip[0].addEventListener('mouseout', () => {
                            this.setState({isDndDisabled: false});
                        });

                }
            }

                });


            document.addEventListener('mousemove', (e) => {
                if (thElm) {
                    const columnClassName = thElm.className.split(' ').find(el => el.indexOf('column-') !== -1);
                    const currentWidth = startOffset + e.pageX;
                    resizeTableColumn(current, columnClassName, currentWidth, 60);

                }
            });

            document.addEventListener('mouseup', (e) => {
                if (thElm) {
                    const columnName = thElm.className.split(' ').find(el => el.indexOf('column-') !== -1);

                    this.setState((state) => {
                        localStorage.setItem('tableStyles', JSON.stringify({
                            ...state.styleObject,
                            [columnName]: { currentWidth: startOffset + e.pageX }
                        }));
                        return {
                            styleObject: {
                                ...state.styleObject,
                                [columnName]: {currentWidth: startOffset + e.pageX}
                            }
                        };
                    });
                    thElm = null;
                }
            });

    }
    }

    componentDidUpdate(prevProps) {
        console.log('CustomTableComponent componentDidUpdate ' + performance.now() + ' milliseconds.');
        const {columns} = this.props;

        if (prevProps.columns.findIndex(el => el.id === 'name') !== columns.findIndex(el => el.id === 'name')) {
            let columns = document.getElementById('search_results_table').childNodes;
            let leftDist = 0;
            let makeFixable = true;
            for (let i = 0, l = columns.length; i < l; i++) {
                const currentNodeWidth = window.getComputedStyle(document.querySelector(`#search_results_table > div:nth-child(${i + 1}`)).width;

                if (makeFixable) {
                    columns[i].style.position = 'sticky';
                    columns[i].style.left = `${leftDist}px`;
                    columns[i].style.zIndex = 10;
                    leftDist = leftDist + parseFloat(currentNodeWidth);
                } else {
                    columns[i].style.position = 'static';
                    columns[i].style.left = 0;
                    columns[i].style.zIndex = 0;
                }

                if (columns[i].getAttribute('data-key') === 'name') {
                    makeFixable = false;
                }
            }
        }
    }

    handleDragEnd = (result) => {
        const {onDragEnd} = this.props;
        this.setState({isDraggingStarted: false,});

        if (!result.destination) {
            return;
        }

        if (onDragEnd) {
            onDragEnd(result);
        }
    };

    handleDragStart = () => {
        this.setState({isDraggingStarted: true,});
    };

    render() {
        const {entities, columns, handleDeleteColumn, handleSorting, resizable} = this.props;
        const {isDndDisabled, styleObject, isDraggingStarted} = this.state;
        console.log('CustomTableComponent render ' + performance.now() + ' milliseconds.');

        return (
            <React.Fragment>
                <DragDropContext onDragEnd={this.handleDragEnd} onDragStart={this.handleDragStart}>
                    <Droppable droppableId='dnd_table_items' direction='horizontal' ignoreContainerClipping>
                        {(provided) => (
                            <div id={'search_results_table'} className={TableStyles.table} {...provided.droppableProps}
                                 ref={provided.innerRef}>
                                {columns.map((column, index) => {
                                    return (
                                        <Draggable key={column.id}
                                                   draggableId={`col-${column.id}`}
                                                   index={index}
                                                   isColumnDisabled={column.isDragDisabled}
                                                   isDragDisabled={isDndDisabled || column.isDragDisabled}
                                        >
                                            {(providedDraggable) => {
                                                return <TableColumn column={column}
                                                                    cellsData={entities}
                                                                    dndProps={providedDraggable}
                                                                    handleDeleteColumn={handleDeleteColumn}
                                                                    handleSorting={handleSorting}
                                                                    index={index}
                                                                    resizable={resizable}
                                                                    isDraggingStarted={isDraggingStarted}
                                                                    currentWidth={styleObject &&
                                                                                  styleObject[`column-${index}`] &&
                                                                                  styleObject[`column-${index}`].currentWidth}
                                                />;
                                            }}
                                        </Draggable>
                                    );
                                })}
                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            </React.Fragment>
        );
    }
}

export default DndTable;
