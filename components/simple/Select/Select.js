/* eslint-disable max-lines */
import React from 'react';
import ChevronDown from '../../../assets/svg/select-chevron-down.svg';
import ChevronUp from '../../../assets/svg/select-chevron-up.svg';
import DeleteOptionIcon from '../../../assets/svg/select-close.svg';
import Check from '../../../assets/svg/select-check.svg';
import SelectStyles from './Select.less';
import classNames from 'classnames/bind';
import {Trans} from 'react-i18next';
import i18next from 'i18next';

let cx = classNames.bind(SelectStyles);

export default class Select extends React.Component {
    constructor(props) {
        super(props);
        const values = props.defaultValue || props.defaultValue === null ? [props.defaultValue] : [];
        this.state = {
            values: values,
            focusedValue: -1,
            // isFocused: false,
            isOpen: false,
        };

        // this.onFocus = this.onFocus.bind(this);
        // this.onBlur = this.onBlur.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onDeleteOption = this.onDeleteOption.bind(this);
        this.onHoverOption = this.onHoverOption.bind(this);
        this.onClickOption = this.onClickOption.bind(this);
        this.renderOption = this.renderOption.bind(this);
        this.outsideClickListener = this.outsideClickListener.bind(this);
    }

    // In case when there is two or more the same components
    UNSAFE_componentWillReceiveProps(nextProps) {
        const {defaultValue} = this.props;
        if (nextProps.defaultValue !== defaultValue) {
            this.setState({values: [nextProps.defaultValue]});
        }
    }

    componentDidMount() {
        const {options} = this.props;
        let longestOption = '';
        options && options.map(option => {
            const value = option.label || option.value || '';
            if (value.toString().length > longestOption.length) {
                longestOption = value;
            }
        });

        const option = document.createElement('div');
        option.className = SelectStyles.option;
        option.style.opacity = '0';
        option.style.height = '0px';
        option.innerText = longestOption;
        this.select.appendChild(option);

        if (option.clientWidth + 22 > this.selection.clientWidth) {
            this.selection.style.width = option.clientWidth + 22 + 'px';
        }
        this.select.removeChild(option);
    }

    // onFocus() {
    //     this.setState({isFocused: true});
    // }

    // onBlur() {
    //     const {options, multiple} = this.props;
    //
    //     this.setState(prevState => {
    //         const {values} = prevState;
    //
    //         if (multiple) {
    //             return {
    //                 focusedValue: -1,
    //                 // isFocused: false,
    //                 isOpen: false
    //             };
    //         }
    //         const value = values[0];
    //         let focusedValue = -1;
    //
    //         if (value) {
    //             focusedValue = options.findIndex(option => option.value === value);
    //         }
    //
    //         return {
    //             focusedValue,
    //             // isFocused: false,
    //             isOpen: false
    //         };
    //     });
    // }

    onKeyDown(e) {
        const {options, multiple, optionsGroups} = this.props;
        const {isOpen} = this.state;

        switch (e.key) {
            case ' ':
                e.preventDefault();
                if (isOpen) {
                    if (multiple) {
                        this.setState(prevState => {
                            const {focusedValue} = prevState;

                            if (focusedValue !== -1) {
                                const [...values] = prevState.values;
                                const value = options[focusedValue].value;
                                const index = values.indexOf(value);

                                if (index === -1) {
                                    values.push(value);
                                } else {
                                    values.splice(index, 1);
                                }

                                return {values};
                            }
                        });
                    }
                } else {
                    this.setState({
                        isOpen: true,
                    });
                }
                break;
            case 'Escape':
            case 'Tab':
                if (isOpen) {
                    e.preventDefault();
                    this.setState({
                        isOpen: false,
                    });
                }
                break;
            case 'Enter':
                this.setState(prevState => ({
                    isOpen: !prevState.isOpen,
                }));
                break;
            case 'ArrowDown':
                e.preventDefault();
                this.setState(prevState => {
                    let {focusedValue} = prevState;

                    if (focusedValue < options.length - 1) {
                        focusedValue++;

                        if (multiple) {
                            return {
                                focusedValue,
                            };
                        }
                        return {
                            values: [options[focusedValue].value],
                            focusedValue,
                        };
                    }
                });
                break;
            case 'ArrowUp':
                e.preventDefault();
                this.setState(prevState => {
                    let {focusedValue} = prevState;

                    if (focusedValue > 0) {
                        focusedValue--;

                        if (multiple) {
                            return {
                                focusedValue,
                            };
                        }
                        return {
                            values: [options[focusedValue].value],
                            focusedValue,
                        };

                    }
                });
                break;
            default:
                if (/^[a-z0-9]$/i.test(e.key)) {
                    const char = e.key;

                    clearTimeout(this.timeout);

                    this.setState((prevState) => {
                        const typed = prevState.typed || '' + char;
                        const regex = new RegExp(`^${typed}`, 'i');
                        // const index = options.findIndex(option => re.test(option.value));
                        // let regex = '/^' + typed + '/i'; //IE don't support new RegExp
                        let index;
                        let optionsList = [];

                        if (optionsGroups) {
                            options.map(group => {
                                optionsList = [...group.options, ...optionsList];
                            });
                            index = optionsList.findIndex(option => regex.test(option.title));
                        } else {
                            index = options.findIndex(option => regex.test(option.value));
                        }

                        if (multiple) {
                            return {
                                focusedValue: index,
                            };
                        }
                        return {
                            values: (options[index] ? [options[index].value] : []) || (optionsList[index] ? [optionsList[index].value] : []),
                            focusedValue: index,
                        };

                    });
                }
                break;
        }
    }

    outsideClickListener(event) {
        if (this.selection && !this.selection.contains(event.target)) {
            this.setState({isOpen: false});
        }
        this.removeClickListener();
    }

    removeClickListener() {
        document.removeEventListener('click', this.outsideClickListener);
    }

    onClick() {
        // Detect clicks outside the selection element because onBlur event in IE11 fires earlier than onCLick
        const {isOpen} = this.state;
        if (isOpen) {
            this.removeClickListener();
        } else {
            document.addEventListener('click', this.outsideClickListener);
        }

        this.setState(prevState => ({isOpen: !prevState.isOpen}));
    }

    onDeleteOption(e) {
        const {value} = e.currentTarget.dataset;

        this.setState(prevState => {
            const [...values] = prevState.values;
            const index = values.indexOf(value);

            values.splice(index, 1);

            return {values};
        });
    }

    onHoverOption(e) {
        const {options} = this.props;
        const {value} = e.currentTarget.dataset;
        const index = options.findIndex(option => option.value === value);

        this.setState({focusedValue: index});
    }

    onClickOption(e, value, option) {
        this.stopPropagation(e);

        const {multiple, onSelectHandler} = this.props;
        // const { value } = e.currentTarget.dataset;

        this.setState(prevState => {
            if (!multiple) {
                onSelectHandler && onSelectHandler(value, option);

                return {
                    values: [value],
                    isOpen: false,
                };
            }

            const [...values] = prevState.values;
            const index = values.indexOf(value);

            if (index === -1) {
                values.push(value);
            } else {
                values.splice(index, 1);
            }

            onSelectHandler(value, option);

            return {values};
        });
    }

    stopPropagation(e) {
        e.nativeEvent.stopImmediatePropagation();
    }

    renderValues() {
        const {placeholder, multiple, options, optionsGroups} = this.props;
        const {values} = this.state;

        if (values && values.length === 0) {
            return (
                <div className={SelectStyles.placeholder}>
                    {placeholder}
                </div>
            );
        }

        if (multiple) {
            let multipleClass = cx({
                'multiple': true,
                'value': true,
            });
            return values.map(value => {
                return (
                    <span
                        key={value}
                        onClick={this.stopPropagation}
                        className={multipleClass}
                    >
            {value}
                        <span
                            data-value={value}
                            onClick={this.onDeleteOption}
                            className={SelectStyles.delete}
                        >
              <DeleteOptionIcon className={SelectStyles.icon}/>
            </span>
          </span>
                );
            });
        }

        let selectedOption;

        if (optionsGroups) {
            let optionsList = [];
            options.map(group => {
                optionsList = [...group.options, ...optionsList];
            });
            selectedOption = optionsList && optionsList.filter(opt => (opt.value && opt.value) === values[0])[0];
        } else {
            selectedOption = options && options.filter(opt => (opt.value && opt.value) === values[0])[0];
        }
        const key = options && options.find(el => el.value === values[0]) && options.find(el => el.value === values[0]).key;
        const key2 =  options && options.find(el => el.value === values[0]) && options.find(el => el.value === values[0]).key2;
        const IconComponent = selectedOption && selectedOption.icon;

        return (
            <div className={SelectStyles.value}>
                {IconComponent && React.cloneElement(IconComponent, {className: SelectStyles.option_icon})}
                {selectedOption && selectedOption.label || (key && <Trans i18nKey={key} defaults={key}/>) || values[0]}
                {key2 && ` (${i18next.t(key2)})`}
            </div>
        );
    }

    renderOptions() {
        const {options, optionsGroups, optionsToTop} = this.props;
        const {isOpen} = this.state;

        let optionsClass = cx({
            'options_to_top' : optionsToTop,
            'options_to_bottom' : !optionsToTop,
        });

        if (!isOpen) {
            return null;
        }

        if (optionsGroups) {
            return (
                <div className={SelectStyles.groups_container}>
                    {options.map(group => {
                        return (
                            <div className={SelectStyles.group} key={group.title}>
                                <div className={SelectStyles.groupTitle}>{group.title}</div>
                                <div className={SelectStyles.group_options}>
                                    {group.options.map(this.renderOption)}
                                </div>
                            </div>
                        );
                    })
                    }
                </div>
            );
        }
        return (
            <div className={optionsClass}>
                {options.map(this.renderOption)}
            </div>
        );
    }

    renderOption(option, index) {
        const {multiple, optionsGroups} = this.props;
        const {values, focusedValue} = this.state;
        const {value, label, key, icon, key2} = option;
        const selected = values.includes(value);

        const IconComponent = icon;

        let optionClass = cx({
            'option': !optionsGroups,
            'group_option': optionsGroups,
            'selected': selected,
            'focused': index === focusedValue,
        });

        return (
            <div
                key={value}
                data-value={value}
                className={optionClass}
                onMouseOver={this.onHoverOption}
                onClick={(e) => this.onClickOption(e, value, option)}
            >
                {IconComponent && React.cloneElement(IconComponent, {className: SelectStyles.option_icon})}
                {multiple
                && <span className={SelectStyles.checkbox}>
                    {selected && <Check className={SelectStyles.icon}/>}
                    </span>
                }
                {label || <Trans i18nKey={key} defaults={key}/> || value}
                {key2 &&' ('}
                {key2 && <Trans i18nKey={key2} defaults={key2}/>}
                {key2 && ')'}
            </div>
        );
    }

    render() {
        const {label} = this.props;
        const {isOpen} = this.state;

        return (
            <React.Fragment>
                {label && <label className={SelectStyles.label}>{label}</label>}
                <div className={SelectStyles.select}
                     tabIndex='0'
                    // onFocus={this.onFocus}
                    // onBlur={this.onBlur}
                     onKeyDown={this.onKeyDown}
                     ref={node => this.select = node}
                >
                    <div className={SelectStyles.selection}
                         onClick={this.onClick}
                         ref={(selection) => this.selection = selection}
                    >
                        {this.renderValues()}
                        <span className={SelectStyles.arrow}>
                        {isOpen ? <ChevronUp className={SelectStyles.icon}/> :
                            <ChevronDown className={SelectStyles.icon}/>}
                        </span>
                    </div>
                    {this.renderOptions()}
                </div>
            </React.Fragment>
        );
    }
}
