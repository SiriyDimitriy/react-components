const {resolve} = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development';

const outputFileName = 'bundle.js';
const outputPath = '../';

const config = {
    entry: {
        js: ['./app.js'],
        vendor: ['react'],
    },
    output: {
        filename: outputFileName,
        path: resolve(__dirname, outputPath),
        publicPath: '/out/',
        // publicPath: '/',
        //hotUpdateChunkFilename: 'hot/hot-update.js',
        //hotUpdateMainFilename: 'hot/hot-update.json',
    },

    resolve: {
        extensions: ['.jsx', '.js', '.json', '.scss', '.sass', '.less', '.css', '.svg'],
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                enforce: 'pre',
                use: [
                    {loader: 'babel-loader'},
                    {loader: 'eslint-loader'},
                    // {loader: 'webpack-conditional-loader'}
                ],
            },
            {
                test: /\.jpg$/,
                exclude: [/node_modules/, /bin/],
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'image/jpg',
                        name: '/assets/[name].[ext]',
                    },
                },
            },
            {
                test: /\.(png|gif)$/,
                exclude: [/node_modules/, /bin/],
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        // mimetype: 'image/png',
                        name: '/assets/[name].[ext]',
                    },
                },
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 100000,
                        mimetype: 'application/font-woff',
                        name: 'fonts/[hash].[ext]',
                    },
                },
            },
            {
                test: /\.(css|less)$/,
                exclude: [/node_modules/, /out/, /styles/],
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            javascriptEnabled: true,
                            sourceMap: true,
                            modules: true,
                            localIdentName: '[local]___[hash:base64:5]',
                        },
                    },
                    {
                        loader: 'less-loader',
                        options: {
                            javascriptEnabled: true,
                        },
                    },
                ],
            },
            {
                test: /\.(css|less)$/,
                include: [/node_modules/, /styles/],
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            javascriptEnabled: true,
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'less-loader',
                        options: {
                            javascriptEnabled: true,
                        },
                    },
                ],
            },
            {
                test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader',
            },
            {
                test: /\.svg$/,
                exclude: [/node_modules/, /bin/],
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env', '@babel/preset-react'],
                        },
                    },
                    {
                        loader: 'react-svg-loader',
                        options: {
                            jsx: true, // true outputs JSX tags
                            //     svgo: {
                            //         plugins: [{removeTitle: false}],
                            //         floatPrecision: 2
                            //     }
                        },
                    },
                ],
            },
        ],
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: 'style.css',
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': isDev ? '"development"' : '"production"',
                'SEARCHENGINE_API_URL': JSON.stringify(process.env.SEARCHENGINE_API_URL),
                'ALTERNATE_APIS': JSON.stringify(process.env.ALTERNATE_APIS),
                'VERSION': JSON.stringify(process.env.VERSION),
                'BUILD_DATE': JSON.stringify(process.env.BUILD_DATE),
            },
            _development_: isDev,
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
    ],
};

config.plugins.push(new CopyWebpackPlugin([
        {
            from: 'assets',
            to: 'assets',
        },
    ],
))

module.exports = config;
