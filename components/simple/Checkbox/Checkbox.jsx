import React from 'react';
import PropTypes from 'prop-types';
import CheckboxStyles from './Checkbox.less';
import ReactTooltip from 'react-tooltip';
import classNames from 'classnames/bind';

let cx = classNames.bind(CheckboxStyles);

export default class Checkbox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleState = this.handleState.bind(this);
    }

    shouldComponentUpdate(nextProps) {
        const {checked} = this.props;
        return checked !== nextProps.checked;
    }

    handleState() {
        const {onToggle} = this.props;
        onToggle && onToggle();
    }

    render() {
        const {label, tooltip, checked, size, disabled, id} = this.props;
        const checkboxClass = cx({
            section: true,
            s: size === 's',
        });
        return (
            <section className={checkboxClass}>
                <label className={CheckboxStyles.agreement} data-tip='' data-for={tooltip}>
                    <span className={CheckboxStyles.label}>{label}</span>
                    <input id={id} type='checkbox' onClick={this.handleState} value={checked} checked={checked}
                           disabled={disabled}/>
                    <div className={CheckboxStyles.indicator}/>
                </label>

                {tooltip && <ReactTooltip id={tooltip}
                                          place='right'
                                          type='light'
                                          effect='solid'
                                          border={true}>
                    {tooltip}
                </ReactTooltip>}
            </section>
        );
    }
}

Checkbox.propTypes = {
    onToggle: PropTypes.func,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    label: PropTypes.string,
    tooltip: PropTypes.string,
    size: PropTypes.string
};
