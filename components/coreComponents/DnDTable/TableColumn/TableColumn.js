import React from 'react';
import ReactTooltip from 'react-tooltip';
import ColumnStyles from './TableColumn.less';
import DeleteIcon from '../../../../assets/svg/cross-icon.svg';
import {Link} from 'react-router-dom';
import {addValueToFilter, setSorting} from '../../../../actions/entities/searchActions';
import {createStructuredSelector} from 'reselect';
import {
    makeSelectSortingDirection, makeSelectSortingField
} from '../../../../selectors/commonSelectors';
import connect from 'react-redux/es/connect/connect';
import AscSortingIcon from '../assets/ascending_sort.svg';
import DescSortingIcon from '../assets/descending_sort.svg';
import TableBodyCell from '../TableBodyCell/TableBodyCell';
import {Trans} from 'react-i18next';
import classNames from 'classnames/bind';

const mapDispatchToProps = dispatch => ({
    handleSorting: (field) => dispatch(setSorting(field)),
    addValueToFilter: (key, value) => dispatch(addValueToFilter(key, value)),
});

let cx = classNames.bind(ColumnStyles);

const mapStateToProps = createStructuredSelector({
    sortBy: makeSelectSortingField(),
    sortOrder: makeSelectSortingDirection(),
});

@connect(mapStateToProps, mapDispatchToProps)
export default class TableColumn extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            columnClassName: cx({
                'table_column': true,
                'table_header': !props.isDraggingStarted,
                [`column-${props.index}`]: true,
            }),
        };
    }

    handleHover = (e) => {
        e.persist();
        const idElement = e.currentTarget.getAttribute('data-id');
        const elementList = document.querySelectorAll(`[data-id = ${idElement}]`);

        for (let i = 0; i < elementList.length; i++) {
            elementList[i].className = ColumnStyles.hovered_element;
        }
       ReactTooltip.hide();
    };

    handleUnhover = (e) => {
        e.persist();
        const idElement = e.currentTarget.getAttribute('data-id');
        const elementList = document.querySelectorAll(`[data-id = ${idElement}]`);

        for (let i = 0; i < elementList.length; i++) {
            elementList[i].className = ColumnStyles.data_cell;
        }
        ReactTooltip.hide();
    };

    handleClickDeleteColumn = () => {
        const {handleDeleteColumn, column} = this.props;

        if (handleDeleteColumn) {
            const {id} = column;
            handleDeleteColumn(id);
        }
    };

    handleClickSorting = () => {
        const {column, handleSorting} = this.props;

        if (column.sorting) {
            handleSorting(column.id);
        }
    };

    render() {
        const {column, cellsData, dndProps, sortBy, sortOrder, addValueToFilter, resizable, currentWidth} = this.props;
        const headerClassName = column.isDragDisabled ? `${ColumnStyles.header_cell} ${ColumnStyles.drag_disabled}` :
            ColumnStyles.header_cell;
        const {columnClassName} = this.state;
        const isSorted = sortBy === column.id;
        const draggableProps = column.isDragDisabled ? {} : dndProps.draggableProps;
        return (
            <div ref={dndProps.innerRef} {...draggableProps} data-key={column.id} className={columnClassName}>
                <div {...dndProps.dragHandleProps} className={headerClassName}>
                    {column.sorting && isSorted && (sortOrder === 'asc') &&
                    <AscSortingIcon className={ColumnStyles.header_cell_sorting_icon}/>}
                    {column.sorting && isSorted && (sortOrder === 'desc') &&
                    <DescSortingIcon className={ColumnStyles.header_cell_sorting_icon}/>}
                    <div className={ColumnStyles.header_label} onClick={this.handleClickSorting} data-tip data-for={column.id}>{column.label}</div>
                    {!column.isDeleteDisabled &&
                    <div className={ColumnStyles.header_actions}>
                        <button className={ColumnStyles.delete_btn} onClick={this.handleClickDeleteColumn}>
                            <DeleteIcon/>
                        </button>
                    </div>}
                    {resizable && (column.id!=='itemNum') && <div className={ColumnStyles.grip} data-element='resizable'>&nbsp;</div>}

                    {!column.sorting &&
                        <ReactTooltip id={column.id}
                                      data-cell='remove-before-convert-to-excel'
                                      place='top'
                                      type='light'
                                      border={true}
                                      event={'click'}
                                      globalEventOff='click'
                                      effect='solid'>
                            <div>
                                <Trans i18nKey={'no_sorting_possible'} defaults={'no_sorting_possible'}/>
                            </div>
                        </ReactTooltip>}
                </div>

                {cellsData.map((item, i) => {
                    const key = `${column.id}_${i}`;

                    const cellId = column.id;
                    const dataType = column.type;
                    const data = item[column.id];
                    const unit = column.unit;
                    const id = 'item' + i;
                    return (
                        <div key={key}
                             className={ColumnStyles.data_cell}
                             data-id={id}
                             onMouseOver={(e) => this.handleHover(e)} onMouseLeave={(e) => this.handleUnhover(e)}
                        >
                            {data && (column.isLink
                                ? <Link to={`/documents/${item.linkTo}`} style={{width: '100%'}} className={ColumnStyles.link_name}>
                                    <TableBodyCell dataType={dataType}
                                                   data={data}
                                                   cellId={cellId}
                                                   item={item}
                                                   id={key}
                                                   unit={unit}
                                                   currentWidth={currentWidth}
                                                   cellIndex={i}
                                    />
                                </Link>
                                : <TableBodyCell dataType={dataType}
                                                 data={data}
                                                 cellId={cellId}
                                                 id={key}
                                                 unit={unit}
                                                 item={item}
                                                 filterable={column.filterable}
                                                 addValueToFilter={addValueToFilter}
                                                 currentWidth={currentWidth}
                                                 cellIndex={i}
                                />)}
                        </div>
                    );
                })}
            </div>
        );
    }
}