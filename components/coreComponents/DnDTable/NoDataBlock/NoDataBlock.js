import React from 'react';
import NoDataBlockStyles from './NoDataBlock.less';

const NoDataBlock = () => <section className={NoDataBlockStyles.no_data_block}>No available data</section>;

export default NoDataBlock;
