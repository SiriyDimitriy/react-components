import React from 'react';
import TableBodyCellStyles from './TableBodyCell.less';
import classNames from 'classnames/bind';
import {
    StringDataTypeComponent,
    LinkDataTypeComponent,
    ImageDataTypeComponent,
} from './tableCellUtils';
import ReactTooltip from 'react-tooltip';

let cx = classNames.bind(TableBodyCellStyles);

class TableBodyCell extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isEllipsisActive: false,
            contentClass: cx({
                'data_container': true,
                'is_link': props.contentIsLink,
                crossed_out: props.textCrossedOut,
            }),
        };
    }

    componentDidMount() {
        const {currentWidth, dataType} = this.props;
        const cellWidth = this.cell && (this.cell.scrollWidth || this.cell.offsetWidth);
        if (dataType === 'String' || dataType === 'Integer' || dataType === 'Real' || dataType === 'Rational') {
            this.setState(() => ({isEllipsisActive: currentWidth < (cellWidth + 30)}));
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {dataType} = this.props;
        const contentWidth = this.cell.querySelector('span > div') && this.cell.querySelector('span > div').scrollWidth;
        const div = this.cell.querySelector('div');
        const divWidth = div && (div.scrollWidth || div.offsetWidth);

        if ((dataType === 'String' || dataType === 'Integer' || dataType === 'Real' || dataType === 'Rational')
            && contentWidth && nextProps.currentWidth && (contentWidth !== nextProps.currentWidth)) {
            this.setState({isEllipsisActive: nextProps.currentWidth < (divWidth + 30)});
            // if (nextProps.currentWidth > width) this.setState({width: null});
        }
    }

    handleClickContent = (value) => {
        const {contentIsLink, itemId, onContentClickHandler, cellData, dataType, filterable, addValueToFilter, cellId} = this.props;
        if (onContentClickHandler && contentIsLink) {
            onContentClickHandler(itemId, dataType, cellData);
        }

        if (filterable && addValueToFilter
            && (dataType === 'String' || dataType === 'Integer' || dataType === 'Real' || dataType === 'Rational')) {
            addValueToFilter(cellId, value);
        }
    };

    render() {
        const {cellId, dataType, data, id, unit, currentWidth, cellIndex} = this.props;
        const {
            isEllipsisActive,
            contentClass,
        } = this.state;

        return (
            <React.Fragment>
                <span data-tip
                      data-for={`${id}`}
                      className={contentClass}
                      ref={(cell) => this.cell = cell}
                    // style={currentWidth ? {width: '100%', textOverflow: 'ellipsis', overflow: 'hidden'} : {}}
                >
                    {(dataType === 'String') &&
                    <StringDataTypeComponent data={data}
                                             cellId={cellId}
                                             isEllipsisActive={isEllipsisActive}
                                             handleClickContent={this.handleClickContent}
                                             currentWidth={currentWidth}
                    />}
                    {(dataType === 'link') &&
                    <LinkDataTypeComponent data={data} cellId={cellId}/>}
                    {(dataType === 'image/jpeg' || dataType === 'image/png') &&
                    <ImageDataTypeComponent data={data} mimeType={dataType} id={id}/>}
                    {(dataType === 'Rational') &&
                    <StringDataTypeComponent data={data} cellId={cellId} handleClickContent={this.handleClickContent}/>}
                    {(dataType === 'Real') &&
                    <StringDataTypeComponent data={data} handleClickContent={this.handleClickContent}/>}
                    {(dataType === 'labelToken' || dataType === 'entityLink') &&
                    <StringDataTypeComponent data={data} cellId={cellId}
                                             isEllipsisActive={isEllipsisActive}/>}
                    {(dataType === 'Integer') &&
                    <StringDataTypeComponent data={data} cellId={cellId} isEllipsisActive={isEllipsisActive}
                                             handleClickContent={this.handleClickContent}/>}
                    {unit && data && <span className={TableBodyCellStyles.unit_measure}>{unit}</span>}

                    {isEllipsisActive &&
                    <div className={TableBodyCellStyles.three_dots_wrapper}
                         style={(cellIndex % 2) ? {backgroundColor: '#FFFFFF'} :
                             {backgroundColor: '#F0F6FD'}}>
                        <img src={require('../../../../assets/jpg/three_dots.png')} alt={'three dots'}
                             style={(cellIndex % 2) ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F0F6FD'}}
                             className={TableBodyCellStyles.three_dots}/>
                    </div>}

                </span>

                {isEllipsisActive && <div data-cell='remove-before-convert-to-excel'>
                    <ReactTooltip id={`${id}`}
                                  place='right'
                                  type='light'
                                  border={true}
                                  effect='solid'>
                        {data && typeof data === 'object' && data.map(el => <React.Fragment
                            key={el}><span>{el}</span>{data.length > 1 && '; '}</React.Fragment>)}
                        {data && typeof data === 'string' && data}
                    </ReactTooltip>
                </div>}
            </React.Fragment>
        );
    }
}

export default TableBodyCell;
