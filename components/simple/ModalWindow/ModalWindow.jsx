import React from 'react';
import ModalPageStyles from './ModalWindow.less';
import PropTypes from 'prop-types';
import CancelIcon from '../../assets/svg/cancel.svg';

class ModalWindow extends React.PureComponent {

    render() {
        const {show, onClose, renderModalHeader, modalButtons, renderModalContent}=this.props;

        if (!show ) {
            return null;
        }

        return(
            <div>
                <div className={ModalPageStyles.modal_dialog}>
                    <div className={ModalPageStyles.modal_content}>
                        <div className={ModalPageStyles.header}>
                            {renderModalHeader()}
                            <div className={ModalPageStyles.close} onClick= {onClose}>
                                <CancelIcon className={ModalPageStyles.cancel_icon}/>
                            </div>
                        </div>
                        <div className={ModalPageStyles.content}>{renderModalContent()}</div>
                        <div className={ModalPageStyles.footer}>
                            {modalButtons}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ModalWindow.propTypes = {
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
};


export default ModalWindow;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    