import React from 'react';
import ViewIcon from '../../../../assets/svg/eye.svg';
import EditIcon from '../../../../assets/svg/edit.svg';
import PdfIcon from '../../../../assets/svg/pdf.svg';
import PlusIcon from '../../../../assets/svg/plus.svg';
import ColumnStyles from './TableBodyCell.less';
import Modal from './components/Modal/Modal';
import ModalStyles from './components/Modal/Modal.less';
import ReactTooltip from 'react-tooltip';

const StringDataTypeComponent = ({data, handleClickContent}) => {
    if (typeof data === 'object') {
        return (
            <div className={ColumnStyles.string_container}>
                {data && data.map(el => <React.Fragment key={el}>
                    <div onClick={() => handleClickContent(el)}>{el}</div>
                    {data.length > 1 && '; '}
                </React.Fragment>)}
            </div>
        );
    }

    if (typeof data === 'string' || typeof data === 'number') {
        return (
            <div>{data}</div>
        );
    }

    return null;
};

const LinkDataTypeComponent = ({data, cellId}) => {
    if (!data || !data.url) {
        return null;
    }

    let knownIcons = {
        'viewItem': <ViewIcon />,
        'EditItem': <EditIcon />,
        'LINK_DOC': <PdfIcon />,
        'requestNewItem': <PlusIcon />,
        'newMaterialRequest': <PlusIcon />,
        'newItemWithItemTemplate': <PlusIcon />,
        'requestWithDraft': <PlusIcon />,
        // The following will be changed later, these are added for a quick fix in borbet
        'revisionItem': <PlusIcon />,
        'requestForMyPlant': <PlusIcon />,
        'changeMtart': <PlusIcon />,
        'withdrawItem': <PlusIcon />
    };

    return (
        <React.Fragment>
            <a href={data.url} className={ColumnStyles.link_icon} target='_blank' rel='noopener noreferrer' aria-label={`${data.key}_icon`}>
                {data.imageLocation &&
                    <img src={data.imageLocation} />}
                {!data.imageLocation && knownIcons[cellId]}
                {!data.imageLocation && !knownIcons[cellId] && `${data.title}`}
            </a>
        </React.Fragment>
    );
};

class ImageDataTypeComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
        };
    }

    imageClick = () => {
        ReactTooltip.hide();
        this.setState({
            isVisible: true,
        });
    };

    handleCancel = () => {
        this.setState({
            isVisible: false,
        });
    };

    render() {
        const {data, mimeType, id} = this.props;
        const {isVisible} = this.state;
        // Render nothing if the "data" prop is null
        if (!data) {
            return null;
        }
        return (
            <div>
                <img className={ColumnStyles.small_image}
                     data-tip data-for={id}
                     src={`data:${mimeType};base64,${data}`}
                     onClick={this.imageClick}
                     alt={'cell image'}
                />

                <ReactTooltip id={id} border={true} type={'light'}>
                    <img className={ColumnStyles.large_image}
                         alt={'cell image'}
                         src={`data:${mimeType};base64,${data}`}
                    />
                </ReactTooltip>

                <Modal
                    show={isVisible}
                    onClose={this.handleCancel}
                >
                    <img className={ModalStyles.image}
                         alt={'modal image'}
                         src={`data:${mimeType};base64,${data}`}
                    />
                </Modal>

            </div>
        );
    }
}

export {
    StringDataTypeComponent,
    LinkDataTypeComponent,
    ImageDataTypeComponent,
};
