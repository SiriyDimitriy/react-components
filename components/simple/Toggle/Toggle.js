import React from 'react';
import ToggleSwitchStyles from './Toggle.less';

const Toggle = ({checked, toggleChangeHandler, label}) => {
    return (
        <section className={ToggleSwitchStyles.wrapper}>
            <label className={ToggleSwitchStyles.switch}>
                <input type={'checkbox'} checked={checked} readOnly onChange={toggleChangeHandler}/>
                <span className={ToggleSwitchStyles.slider}/>
            </label>
            <span className={ToggleSwitchStyles.label}>{label}</span>
        </section>
    );
};

export default Toggle;
