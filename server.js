const express = require('express');
// const proxy = require('http-proxy-middleware');
const app = express();
const path = require('path');
const port = process.env.PORT;

const proxyConfig = {
    target: 'http://alpha.idl.paradine.at:9080',
    logLevel: 'debug',
    pathRewrite: {
        // '^/api': '', // rewrite path
    },
    onProxyRes(proxyRes) {
        if (
            proxyRes.headers.location &&
            proxyRes.headers.location.includes(proxyConfig.target)
        )
            proxyRes.headers.location = proxyRes.headers.location.replace(
                proxyConfig.target,
                'http://localhost:4005',
            );
    },
    onProxyReq(proxyReq, req, res) {
        // console.log('----------------------------------------------', proxyReq);
        // if (req.path.includes(`${appBase}/react-apps`)) {
        //     // console.log('----------------------------------------------replace');
        //     // req.url.replace(`${appBase}/react-apps`, '/out');
        //     // console.log(req.url);
        //
        //     return false;
        // }
    },
    changeOrigin: true,
};

const locales = ['en_US', 'de_DE'];

locales.forEach(locale => {
    app.get(`/locales/${locale}/messages.json`, (req, res) => res.sendFile(path.join(__dirname + `/locales/${locale}/messages.json`)));
});

app.use('/out', express.static('out'));

app.use('/out', express.static(path.join(__dirname, 'out')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, './index.html'));
});

app.listen(port, function () {
    console.log(`node server started on ${port}`);
    // console.log('app base', appBase);
});