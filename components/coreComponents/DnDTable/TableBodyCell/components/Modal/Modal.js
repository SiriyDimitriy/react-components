import React from 'react';
import PropTypes from 'prop-types';
import ModalStyles from './Modal.less';
import DeleteIcon from '../../../../../../assets/svg/cross-icon.svg';

class Modal extends React.PureComponent {
  render() {
    const {show, onClose, children}=this.props;
    // Render nothing if the "show" prop is false
    if (!show ) {
      return null;
    }
    return (
      <div className={ModalStyles.background_modal}>
        <div className={ModalStyles.image_modal}>
          <button className={ModalStyles.cancel_btn} onClick={onClose}>
            <DeleteIcon className={ModalStyles.cancel_icon}/>
          </button>
          {children}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;