import React from 'react';
import MainPage from '../pages/MainPage/MainPage';

class AppContent extends React.Component {
    render() {
        return (
            <React.Fragment>
                <MainPage/>
            </React.Fragment>
        );
    }
}

export default AppContent;
