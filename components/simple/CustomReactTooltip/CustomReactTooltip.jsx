import React from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import CustomTooltipStyles from './CustomReactTooltip.less';
import ReactDOM from 'react-dom';

class CustomReactTooltip extends React.PureComponent {
    createMarkup() {
        const { renderHTML } = this.props;
        return { __html: renderHTML };
    }

    renderInPortal() {
        return (ReactDOM.createPortal(
            <CustomReactTooltip {...this.props} renderInPortal={false}/>,
            document.body,
        ));
    }

    correctPosition = (e) => {
        const { tooltipId } = this.props;
        const hoverTarget = document.querySelector(`[data-for=${tooltipId}]`);
        const tooltip = this.tooltip = document.getElementById(tooltipId);
        const boxOfHoverTarget = hoverTarget.getBoundingClientRect();
        tooltip.style.zIndex = '-1';
        tooltip.style.top = boxOfHoverTarget.top + hoverTarget.offsetHeight + 'px';
        tooltip.style.left = e.clientX + 'px';
        // the timeout is needed for correct positioning
        this.timeout = setTimeout(() => {
            const centerOfTooltip = tooltip.offsetWidth / 2;
            tooltip.style.left = e.clientX - centerOfTooltip + 'px';
            tooltip.style.zIndex = '999';
        }, 50);
    };

    // to prevent blinking of the tooltip (PCPMDEV-1505)
    resetStyle = () => {
        // this.tooltip.style.zIndex = '';
        clearTimeout(this.timeout);
    };

    render() {
        const { tooltipId, place, children, offset, renderHTML, renderInPortal, centerViaParent } = this.props;
        return (
            renderInPortal ?
                this.renderInPortal() :
                <ReactTooltip id={tooltipId}
                              place={place}
                              effect='solid'
                              className={CustomTooltipStyles.reset_style}
                              offset={offset}
                              afterShow={centerViaParent && this.correctPosition}
                              afterHide={centerViaParent && this.resetStyle}
                >
                    <div
                        className={`${CustomTooltipStyles.customize} __react_component_tooltip type-light border place-${place}`}
                        onClick={(e) => e.stopPropagation()}
                    >
                        {children && <div>{children}</div>}
                        {renderHTML && <div dangerouslySetInnerHTML={this.createMarkup()}/>}
                    </div>
                </ReactTooltip>
        );
    }
}

export default CustomReactTooltip;

CustomReactTooltip.propTypes = {
    place: PropTypes.string,
    tooltipId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    offset: PropTypes.object,
    children: PropTypes.any,
    renderInPortal: PropTypes.bool,
};
