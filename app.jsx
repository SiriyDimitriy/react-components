import React from 'react';
import ReactDOM from 'react-dom';
import './styles/resetCSS.css';
import './styles/common.less';
// import 'url-search-params-polyfill'; //IE support of search params

// if (!global._babelPolyfill) { //fix issue for the multiple instances
//     require('babel-polyfill');
// }

import Application from './components/Application';

const render = () => {
    ReactDOM.render(
        <Application/>,
        document.getElementById('root'))
};

window.onload = () => { //load styles before js-files interpretation with browser
    render();
};

if (module.hot) {
    module.hot.accept();
    window.onload = () => { //load styles before js-files interpretation with browser
        render();
    };
}