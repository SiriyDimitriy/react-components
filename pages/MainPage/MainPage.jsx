import React from 'react';
import ActionButton from '../../components/simple/Button/Button';
import Checkbox from '../../components/simple/Checkbox/Checkbox';
import Toggle from '../../components/simple/Toggle/Toggle';

const MainPage = () => {
    return (
        <div className='main_page flex-column'>
            <h3>Main page</h3>
            <h4>Buttons:</h4>
            <ul>
                <li className='flex'>Default button: <ActionButton text={'Default button'}/></li>
                <li className='flex'>Success button: <ActionButton text={'Success button'} type={'success'}/></li>
                <li className='flex'>Disabled button: <ActionButton text={'Disabled button'} disabled={true}/></li>
                <li className='flex'>Clear button: <ActionButton text={'Clear button'} type={'clear'}/></li>
            </ul>
            <h4>Checkboxes</h4>
            <div>
                <Checkbox label={'Checkbox label'}/>
            </div>
            <h4>Toggle elements</h4>
            <div>
                <Toggle label={'Checkbox label'}/>
            </div>
        </div>
    );
};

export default MainPage;