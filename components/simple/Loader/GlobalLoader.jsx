import React from 'react';
import ReactDOM from 'react-dom';
import Loader from './Loader';
import LoaderStyles from './Loader.less';

const GlobalLoader = (props) => {
    return ReactDOM.createPortal(
        <div className={LoaderStyles.global_loader_wrapper}>
            <Loader {...props}/>
        </div>,
        document.body
    );
};

export default GlobalLoader;
