import React from 'react';
import classNames from 'classnames/bind';
import InputStyles from './InputElement.less';

let cx = classNames.bind(InputStyles);

let inputClass = cx({
    'input': true,
});

class InputElement extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value || '',
            valuesList: null,
            focusedValue: null,
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.onFocusHandler = this.onFocusHandler.bind(this);
        this.onAutocompleteClickHandler = this.onAutocompleteClickHandler.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.clickOutside = this.clickOutside.bind(this);

        this.autocompleteContainer = React.createRef();
    }

    onKeyDown(e) {
        const {valuesList, focusedValue, value} = this.state;
        const {onAutocompleteSelectHandler} = this.props;

        switch (e.key) {
            case 'Escape':
            case 'Tab':
                if (valuesList) {
                    e.preventDefault();
                    this.setState({
                        valuesList: null,
                    });
                }
                break;
            case 'Enter':
                e.stopPropagation();
                const selected = focusedValue && focusedValue.replace(/<\/?[^>]+(>|$)/g, '') || value;
                this.setState({value: selected},
                    () => onAutocompleteSelectHandler && onAutocompleteSelectHandler(selected)
                );
                break;
            case 'ArrowDown':
                e.preventDefault();
                this.setState(prevState => {
                    let {focusedValue} = prevState;
                    if (valuesList) {
                        const prevFocusedValueIndex = valuesList.indexOf(focusedValue);

                        if (prevFocusedValueIndex < valuesList.length - 1) {
                            focusedValue = valuesList[prevFocusedValueIndex + 1];

                            return {
                                // values: [options[focusedValue].value],
                                focusedValue,
                            };
                        }
                    }
                });
                break;
            case 'ArrowUp':
                e.preventDefault();
                this.setState(prevState => {
                    let {focusedValue} = prevState;
                    if (valuesList) {
                        const prevFocusedValueIndex = valuesList.indexOf(focusedValue);

                        if (prevFocusedValueIndex !== 0 && prevFocusedValueIndex <= valuesList.length - 1) {
                            focusedValue = valuesList[prevFocusedValueIndex - 1];

                            return {
                                // values: [options[focusedValue].value],
                                focusedValue,
                            };
                        }
                    }
                });
                break;
        }
    }

    clickOutside(e) {
        if (this.autocompleteContainer && this.autocompleteContainer.current && !this.autocompleteContainer.current.contains(e.target)) {
            this.setState({valuesList: null});
        }
    }

    componentDidMount() {
        window.addEventListener('click', this.clickOutside);
    }

    componentWillUnmount() {
        window.removeEventListener('click', this.clickOutside);
    }

    changeHandler(e) {
        const {onChangeHandler} = this.props;
        const {value} = e.target;
        console.log('input change called with value ', value);

        if (value !== '') {
            onChangeHandler && onChangeHandler(value).then(valuesList => {
                console.log('value list arrived ', valuesList);

                this.setState({valuesList});
            });
        } else {
            onChangeHandler && onChangeHandler(e.target.value);
        }

        this.setState({value});
    }

    onFocusHandler() {
        const {onFocusHandler} = this.props;
        onFocusHandler && onFocusHandler().then(res => this.setState({valuesList: res}));
    }

    onAutocompleteClickHandler(e) {
        const {onAutocompleteSelectHandler} = this.props;
        const value = e.target.innerHTML.replace(/<\/?[^>]+(>|$)/g, '');

        this.setState({
            value: value,
            valuesList: null
        }, () => onAutocompleteSelectHandler && onAutocompleteSelectHandler(value));

    }

    render() {
        const {placeholder, type, onBlur, className, width, hasError, errorMessage, border} = this.props;
        const {value, valuesList, focusedValue} = this.state;

        let inputFieldClass = cx({
            'input_field': true,
            input_error: hasError,
            [className]: className,
            'input_bordered': border,
        });

        return (
            <div className={inputFieldClass} style={{width: width}} onKeyDown={this.onKeyDown}>
                <input
                    ref={el => (this.input = el)}
                    type={type || 'text'}
                    autoComplete={'new-password'} // disable autofill
                    placeholder={placeholder}
                    className={inputClass}
                    value={value}
                    onChange={this.changeHandler}
                    onFocus={this.onFocusHandler}
                    onBlur={onBlur}
                />
                {hasError && <span
                    className={InputStyles.input_field_error}>{errorMessage || 'This field contains incorrect value'}</span>}
                {valuesList
                && <div className={InputStyles.autocomplete_section} ref={this.autocompleteContainer}>
                    {valuesList.map(el => <div key={el}
                                               className={InputStyles.autocomplete_item}
                                               style={el === focusedValue ? {backgroundColor: '#4EBDEE'} : {}}
                                               onClick={this.onAutocompleteClickHandler}
                                               dangerouslySetInnerHTML={{__html: el}}/>)}
                </div>}
            </div>
        );
    }
}

export default InputElement;
