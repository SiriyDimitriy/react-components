import React from 'react';
import Styles from './TextComponent.less';

const TextComponent = ({ id, isChangeRequest }) => <img src={'/pcpm/img/delete.png'}
                                                        alt={'remove value'}
                                                        onClick={() => this.onDeleteHandler(id, isChangeRequest)}
                                                        className={Styles.remove_button}/>;

export {
    TextComponent,
};