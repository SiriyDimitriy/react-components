## Intro

Boilerplate contains React, Redux and Redux-saga (can be changed for another middleware like Redux-thunk, etc.).
Boilerplate provides possibility to create FE project from scratch.
Based on:
1. React
2. Redux and Redux-logger (configured)
3. Redux-saga (configured)
4. React-router (configured)
5. i18-next (configured)
6. Reselect

Contains: 
1. Utils for cookies
2. Utils for getting browser versions
3. HTTPService for sending requests to API
4. Reset CSS file

Just clone this repo to start a new project.
For YOUR custom project you can add or remove (to decrease size of bundle.js) libraries

## Setup

1. Install [Node.js](https://nodejs.org/) (v6.0+)
2. Go to project folder and run `npm install`

For development
4. `npm run dev`
5. Go to `http://localhost:4005`

For production
4. `npm run build_app` if you need to get js-file in '/out'

## Configs

* Client hot module reloading
* Custom Babel presets with ES6 support, optional React support and optimizations for polyfilling Node and browser builds.
* CSS Modules and LESS support
* Static asset support
* Inline SVG support
* Style and script linter rulesets
* Infrastructure for testing (configured)

## Command line

There is few possible options to run code, both setted as scripts in package.json:

- for development purposes:
```
npm run dev
```
- for production:

```
npm run build_app
```

Here are all available commands:

* [`dev`]() starts a common development environment
* [`build_app`]() compiles server and client code for production use


for docker

docker build -t searchengine-frontend:1.0.0 .
docker run -p 4005:4005 --name sefecontainer -i -t searchengine-frontend:1.0.0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    